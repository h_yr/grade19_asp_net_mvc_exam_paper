﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ExamDemo.Models;

namespace ExamDemo.Data
{
    public class DataInitlizer : DropCreateDatabaseIfModelChanges<ExamDemoDb>
    {
        protected override void Seed(ExamDemoDb db)
        {
            db.Users.AddRange(new List<Users>
            {
                new Users
                {
                    Username="admin",
                    Password="123456"
                },new Users
                {
                    Username="黄炎榕",
                    Password="123456"
                },new Users
                {
                    Username="曾婷",
                    Password="123456"
                },new Users
                {
                    Username="薛晨达",
                    Password="123456"
                },new Users
                {
                    Username="李泽",
                    Password="123456"
                }
            });

            db.Messages.AddRange(new List<Messages>
            {
                new Messages
                {
                    FromUserId=1,
                    Content="我是超级管理者"
                },new Messages
                {
                    FromUserId=2,
                    Content="My name is Mr.黄"
                },new Messages
                {
                    FromUserId=3,
                    Content="嘤嘤嘤"
                },new Messages
                {
                    FromUserId=4,
                    Content="呜呜呜"
                },new Messages
                {
                    FromUserId=5,
                    Content="啦啦啦"
                },new Messages
                {
                    FromUserId=2,
                    Content="踩踩踩"
                },new Messages
                {
                    FromUserId=4,
                    Content="冲冲冲"
                }
            });

            db.Comments.AddRange(new List<Comments>
            {
                new Comments
                {
                    FromUserId=1,
                    MsgId=2,
                    Comment="哈哈哈"
                },new Comments
                {
                    FromUserId=4,
                    MsgId=5,
                    Comment="啦啦啦"
                },new Comments
                {
                    FromUserId=3,
                    MsgId=2,
                    Comment="嘻嘻嘻"
                },new Comments
                {
                    FromUserId=2,
                    MsgId=4,
                    Comment="呼呼呼"
                },new Comments
                {
                    FromUserId=3,
                    MsgId=3,
                    Comment="呵呵呵"
                },new Comments
                {
                    FromUserId=5,
                    MsgId=1,
                    Comment="略略略"
                },
            });

            db.SaveChanges();

            base.Seed(db);
        }
    }
}