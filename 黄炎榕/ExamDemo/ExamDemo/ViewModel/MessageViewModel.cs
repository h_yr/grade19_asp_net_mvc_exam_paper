﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.ViewModel
{
    public class MessageViewModel
    {
        // 说说的Id
        public int Id { get; set; }
        // 说说的内容
        public string Content { get; set; }
        // 评论列表
        public IEnumerable<CommentViewModel> CommentViewModels { get; set; }
    }
}