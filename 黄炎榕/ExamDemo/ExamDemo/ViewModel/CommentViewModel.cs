﻿namespace ExamDemo.ViewModel
{
    public class CommentViewModel
    {
        // 评论者的用户名
        public string FromUsername { get; set; }
        // 被评论者的用户名（也就是发布说说的用户名）
        public string ToUsername { get; set; }
        // 评论的内容
        public string Comment { get; set; }
    }
}