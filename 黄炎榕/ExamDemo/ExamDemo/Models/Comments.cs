﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.Models
{
    public class Comments : BaseModel
    {
        public int FromUserId { get; set; }
        public int MsgId { get; set; }
        public string Comment { get; set; }
    }
}