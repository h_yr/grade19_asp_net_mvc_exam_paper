﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamDemo.Data;
using ExamDemo.Models;
using ExamDemo.ParamModel;
using Newtonsoft.Json;

namespace ExamDemo.Controllers
{
    public class UsersController : Controller
    {
        public ExamDemoDb db = new ExamDemoDb();

        // GET: Users
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性；有关
        // 更多详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Username,Password,CreatedAt,UpdatedAt,Version,Remarks")] Users users)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(users);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(users);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: Users/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性；有关
        // 更多详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Username,Password,CreatedAt,UpdatedAt,Version,Remarks")] Users users)
        {
            if (ModelState.IsValid)
            {
                db.Entry(users).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(users);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Users users = db.Users.Find(id);
            db.Users.Remove(users);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // 登录
        public ActionResult Login()
        {
            return View();
        }
        // 注册
        public ActionResult Register()
        {
            return View();
        }

        // 登录判断
        [HttpPost]
        public string LoginDone(LoginModel loginModel)
        {
            dynamic result;
            var username = loginModel.Username.Trim();
            var password = loginModel.Password.Trim();

            if (username == "" && password == "")
            {
                result = new
                {
                    code = 1000,
                    msg = "用户名和密码都不能为空！"
                };
            }
            else if (username == "")
            {
                result = new
                {
                    code = 1000,
                    msg = "用户名不能为空！"
                };
            }
            else if (password == "")
            {
                result = new
                {
                    code = 1000,
                    msg = "密码不能为空！"
                };
            }
            else
            {
                var uName = db.Users.Where(x => x.Username == username).FirstOrDefault();
                // 如果用户名不为空，也就是用户存在，则获取密码
                if (uName != null)
                {
                    var uPassword = db.Users.Where(x => x.Password == password).FirstOrDefault();
                    // 密码正确
                    if (uPassword != null)
                    {
                        result = new
                        {
                            code = 200,
                            msg = "登录成功！"

                        };
                        Session["Username"] = username;
                        Session["Id"] = uName.Id;
                    }
                    else
                    {
                        result = new
                        {
                            code = 1000,
                            msg = "密码错误！"
                        };
                    }
                }
                else
                {
                    result = new
                    {
                        code = 1000,
                        msg = "用户不存在，请先注册！"
                    };
                }
            }
            return JsonConvert.SerializeObject(result);
        }

        // 注册判断
        [HttpPost]
        public string RegisterDone(RegisterModel registerModel)
        {
            dynamic result;

            var username = registerModel.Username.Trim();
            var password = registerModel.Password.Trim();
            var confirmPassword = registerModel.ConfirmPassword.Trim();

            if (username.Length > 0 && password.Length > 0 && confirmPassword.Length > 0 && password == confirmPassword)
            {
                var user = db.Users.Where(x => x.Username == username).FirstOrDefault();

                if (user != null)
                {
                    result = new
                    {
                        code = 1000,
                        msg = "当前用户已注册！"
                    };
                }
                else
                {
                    db.Users.Add(new Users
                    {
                        Username = username,
                        Password = password
                    });

                    db.SaveChanges();

                    result = new
                    {
                        code = 200,
                        msg = "恭喜，注册成功！"
                    };
                }
            }
            else
            {
                result = new
                {
                    code = 1000,
                    msg = "用户名或密码不能为空，且两次密码必须一致，请核对后重试！"
                };
            }

            return JsonConvert.SerializeObject(result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
